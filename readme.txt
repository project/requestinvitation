
SYNOPSIS
--------

For semi-private sites or sites which are still in beta require users who are interested in the product to first
request for an invitation for the site and administrators can send an invitation after moderating the user request
list. This module is prepared focusing on this purpose. 

REQUIREMENTS
------------

  - Invite module: http://drupal.org/project/invite

GENERAL USE
-----------

To use the module:

  1. First switch on the 'requestinvitation' block from admin->block.
  3. Users who request invitation should use the 'Request Invitation' block.
  4. Requested users will be listed in admin->configuration->Request Invitation.
  5. Check the users who should be granted invitations. 

CONTACT
-------

For bug reports, feature suggestions and latest developments visit the project page: http://drupal.org/project/requestinvitation

Original Author:
Buddhika Laknath  <blaknath@gmail.com>

